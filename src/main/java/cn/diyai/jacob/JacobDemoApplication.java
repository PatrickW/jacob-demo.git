package cn.diyai.jacob;


import com.jacob.com.LibraryLoader;

public class JacobDemoApplication {
	public static void main(String[] args) throws InterruptedException {
		String jacobDllVersionToUse = JacobDemoApplication.class.getClassLoader().getResource("jacob-1.20-x64.dll").getPath();
		if (!System.getProperty("os.arch").equals("amd64")) {
			jacobDllVersionToUse = JacobDemoApplication.class.getClassLoader().getResource("jacob-1.20-x86.dll").getPath();
		}
		System.setProperty(LibraryLoader.JACOB_DLL_PATH, jacobDllVersionToUse);

		AutoItX autoIt = new AutoItX();
		// 打开记事本应用
        autoIt.run("notepad.exe", "", AutoItX.SW_SHOW);
	}
}

package cn.diyai.jacob;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;


public class OfficeConvertPDF {
    public static void main(String[] args) {
        String wordPath = "xxx.docx";
        String pdfPath = "xxx.pdf";
        ActiveXComponent app = null;
        try{
            app = new ActiveXComponent("Word.application");
            app.setProperty("Visible",false);
            Dispatch docs = app.getProperty("Documents").toDispatch();
            Dispatch doc = Dispatch.call(docs,"Open",wordPath).toDispatch();
            Dispatch.call(doc,"SaveAs",pdfPath,17);
            Dispatch.call(doc,"Close");
        }catch (Exception ex){
            ex.printStackTrace();
        }
        app.invoke("Quit");
    }
}
